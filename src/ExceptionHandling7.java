import java.io.IOException;

class ExceptionHandling7 {
    void m() throws IOException {
        throw new IOException("device error");//checked exception
    }

    void n() throws IOException {
        m();
    }

    void p() {
        try {
            n();
        } catch (Exception e) {
            System.out.println("exception handled");
        }
    }

    public static void main(String args[]) {
        ExceptionHandling7 obj = new ExceptionHandling7();
        obj.p();
        System.out.println("normal flow...");
    }
}