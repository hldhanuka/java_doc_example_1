class InstanceInitializerBlock2 {
    int speed;

    InstanceInitializerBlock2() {
        System.out.println("constructor is invoked");
    }

    {
        System.out.println("instance initializer block invoked");
    }

    public static void main(String args[]) {
        InstanceInitializerBlock2 b1 = new InstanceInitializerBlock2();
        InstanceInitializerBlock2 b2 = new InstanceInitializerBlock2();
    }
}