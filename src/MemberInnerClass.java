class MemberInnerClass {
    private int data = 30;

    class MemberInnerClassInner {
        void msg() {
            System.out.println("data is " + data);
        }
    }

    public static void main(String args[]) {
        MemberInnerClass obj = new MemberInnerClass();
        MemberInnerClass.MemberInnerClassInner in = obj.new MemberInnerClassInner();
        in.msg();
    }
}