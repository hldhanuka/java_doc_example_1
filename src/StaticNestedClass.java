class StaticNestedClass {
    static int data = 30;

    static class StaticNestedClassInner {
        void msg() {
            System.out.println("data is " + data);
        }
    }

    public static void main(String args[]) {
        StaticNestedClass.StaticNestedClassInner obj = new StaticNestedClass.StaticNestedClassInner();
        obj.msg();
    }
}