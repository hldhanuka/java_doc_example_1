class RuntimePolymorphism4Animal {
    void eat() {
        System.out.println("eating");
    }
}

class RuntimePolymorphism4Dog extends RuntimePolymorphism4Animal {
    void eat() {
        System.out.println("eating fruits");
    }
}

class RuntimePolymorphism4 extends RuntimePolymorphism4Dog {
    void eat() {
        System.out.println("drinking milk");
    }

    public static void main(String args[]) {
        RuntimePolymorphism4Animal a1, a2, a3;
        a1 = new RuntimePolymorphism4Animal();
        a2 = new RuntimePolymorphism4Dog();
        a3 = new RuntimePolymorphism4();
        a1.eat();
        a2.eat();
        a3.eat();
    }
}