import java.time.OffsetTime;

public class JavaOffsetTime3 {
    public static void main(String[] args) {
        OffsetTime offset = OffsetTime.now();
        int m = offset.getMinute();
        System.out.println(m + " minute");
    }
}