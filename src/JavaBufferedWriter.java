import java.io.*;

public class JavaBufferedWriter {
    public static void main(String[] args) throws Exception {
        FileWriter writer = new FileWriter("JavaBufferedWritertestout.txt");
        BufferedWriter buffer = new BufferedWriter(writer);
        buffer.write("Welcome to javaTpoint.");
        buffer.close();
        System.out.println("Success");
    }
}