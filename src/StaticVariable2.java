//Java  Program  to  demonstrate  the  use  of  an  instance  variable    
//which  get  memory  each  time  when  we  create  an  object  of  the  class.    
class StaticVariable2 {
    int count = 0;//will  get  memory  each  time  when  the  instance  is  created

    StaticVariable2() {
        count++;//incrementing  value    
        System.out.println(count);
    }

    public static void main(String args[]) {
        //Creating  objects    
        StaticVariable2 c1 = new StaticVariable2();
        StaticVariable2 c2 = new StaticVariable2();
        StaticVariable2 c3 = new StaticVariable2();
    }
}