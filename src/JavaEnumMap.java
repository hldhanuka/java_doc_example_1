import java.util.*;

public class JavaEnumMap {
    // create an enum
    public enum JavaEnumMapDays {
        Monday, Tuesday, Wednesday, Thursday
    }

    public static void main(String[] args) {
        //create and populate enum map
        EnumMap<JavaEnumMapDays, String> map = new EnumMap<JavaEnumMapDays, String>(JavaEnumMapDays.class);
        map.put(JavaEnumMapDays.Monday, "1");
        map.put(JavaEnumMapDays.Tuesday, "2");
        map.put(JavaEnumMapDays.Wednesday, "3");
        map.put(JavaEnumMapDays.Thursday, "4");
        // print the map
        for (Map.Entry m : map.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
    }
}