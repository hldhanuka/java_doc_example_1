import java.time.*;

public class JavaMonthDay {
    public static void main(String[] args) {
        MonthDay month = MonthDay.now();
        LocalDate date = month.atYear(1994);
        System.out.println(date);
    }
}