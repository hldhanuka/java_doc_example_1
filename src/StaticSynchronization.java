class StaticSynchronizationTable {
    synchronized static void printTable(int n) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(n * i);
            try {
                Thread.sleep(400);
            } catch (Exception e) {
            }
        }
    }
}

class StaticSynchronizationMyThread1 extends Thread {
    public void run() {
        StaticSynchronizationTable.printTable(1);
    }
}

class StaticSynchronizationMyThread2 extends Thread {
    public void run() {
        StaticSynchronizationTable.printTable(10);
    }
}

class StaticSynchronizationMyThread3 extends Thread {
    public void run() {
        StaticSynchronizationTable.printTable(100);
    }
}

class StaticSynchronizationMyThread4 extends Thread {
    public void run() {
        StaticSynchronizationTable.printTable(1000);
    }
}

public class StaticSynchronization {
    public static void main(String t[]) {
        StaticSynchronizationMyThread1 t1 = new StaticSynchronizationMyThread1();
        StaticSynchronizationMyThread2 t2 = new StaticSynchronizationMyThread2();
        StaticSynchronizationMyThread3 t3 = new StaticSynchronizationMyThread3();
        StaticSynchronizationMyThread4 t4 = new StaticSynchronizationMyThread4();
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}