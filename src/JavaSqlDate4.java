// important import statement  

import java.util.*;
import java.time.*;

public class JavaSqlDate4 {
    // main method
    public static void main(String[] argvs) {
        // Getting the instance of LocalDateTime  
        LocalDateTime dtm = LocalDateTime.now();
        // Getting the LocalDate representation of the LocalDateTime  
// using the toLocalDate() method  
        System.out.println("The date is: " + dtm.toLocalDate());
    }
}