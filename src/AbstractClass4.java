interface AbstractClass4A {
    void a();

    void b();

    void c();

    void d();
}

abstract class AbstractClass4B implements AbstractClass4A {
    public void c() {
        System.out.println("I am c");
    }
}

class AbstractClass4M extends AbstractClass4B {
    public void a() {
        System.out.println("I am a");
    }

    public void b() {
        System.out.println("I am b");
    }

    public void d() {
        System.out.println("I am d");
    }
}

class AbstractClass4 {
    public static void main(String args[]) {
        AbstractClass4A a = new AbstractClass4M();
        a.a();
        a.b();
        a.c();
        a.d();
    }
}