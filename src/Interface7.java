//Creating interface that has 4 methods  
interface Interface7A {
    void a();//bydefault, public and abstract

    void b();

    void c();

    void d();
}

//Creating abstract class that provides the implementation of one method of A interface  
abstract class Interface7B implements Interface7A {
    public void c() {
        System.out.println("I am C");
    }
}

//Creating subclass of abstract class, now we need to provide the implementation of rest of the methods  
class Interface7M extends Interface7B {
    public void a() {
        System.out.println("I am a");
    }

    public void b() {
        System.out.println("I am b");
    }

    public void d() {
        System.out.println("I am d");
    }
}

//Creating a test class that calls the methods of A interface  
class Interface7 {
    public static void main(String args[]) {
        Interface7A a = new Interface7M();
        a.a();
        a.b();
        a.c();
        a.d();
    }
}