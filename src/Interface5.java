interface Interface5Drawable {
    void draw();

    default void msg() {
        System.out.println("default method");
    }
}

class Interface5Rectangle implements Interface5Drawable {
    public void draw() {
        System.out.println("drawing rectangle");
    }
}

class Interface5 {
    public static void main(String args[]) {
        Interface5Drawable d = new Interface5Rectangle();
        d.draw();
        d.msg();
    }
}