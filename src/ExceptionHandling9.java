import java.io.*;

class ExceptionHandling9M {
    void method() throws IOException {
        System.out.println("device operation performed");
    }
}

class ExceptionHandling9 {
    public static void main(String args[]) throws IOException {//declare exception
        ExceptionHandling9M m = new ExceptionHandling9M();
        m.method();

        System.out.println("normal flow...");
    }
}