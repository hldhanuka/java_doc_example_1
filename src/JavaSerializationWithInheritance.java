import java.io.*;

class JavaSerializationWithInheritancePerson implements Serializable {
    int id;
    String name;

    JavaSerializationWithInheritancePerson(int id, String name) {
        this.id = id;
        this.name = name;
    }
}

class JavaSerializationWithInheritanceStudent extends JavaSerializationWithInheritancePerson {
    String course;
    int fee;

    public JavaSerializationWithInheritanceStudent(int id, String name, String course, int fee) {
        super(id, name);
        this.course = course;
        this.fee = fee;
    }
}

public class JavaSerializationWithInheritance {
    public static void main(String args[]) {
        try {
            //Creating the object
            JavaSerializationWithInheritanceStudent s1 = new JavaSerializationWithInheritanceStudent(211, "ravi", "Engineering", 50000);
            //Creating stream and writing the object
            FileOutputStream fout = new FileOutputStream("JavaSerializationWithInheritancef.txt");
            ObjectOutputStream out = new ObjectOutputStream(fout);
            out.writeObject(s1);
            out.flush();
            //closing the stream
            out.close();
            System.out.println("success");
        } catch (Exception e) {
            System.out.println(e);
        }
        try {
            //Creating stream to read the object
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("JavaSerializationWithInheritancef.txt"));
            JavaSerializationWithInheritanceStudent s = (JavaSerializationWithInheritanceStudent) in.readObject();
            //printing the data of the serialized object
            System.out.println(s.id + " " + s.name + " " + s.course + " " + s.fee);
            //closing the stream
            in.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}