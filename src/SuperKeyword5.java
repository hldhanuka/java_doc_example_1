class SuperKeyword5Person {
    int id;
    String name;

    SuperKeyword5Person(int id, String name) {
        this.id = id;
        this.name = name;
    }
}

class SuperKeyword5Emp extends SuperKeyword5Person {
    float salary;

    SuperKeyword5Emp(int id, String name, float salary) {
        super(id, name);//reusing parent constructor
        this.salary = salary;
    }

    void display() {
        System.out.println(id + " " + name + " " + salary);
    }
}

class SuperKeyword5 {
    public static void main(String[] args) {
        SuperKeyword5Emp e1 = new SuperKeyword5Emp(1, "ankit", 45000f);
        e1.display();
    }
}