import java.sql.*;

class JavaDatabasePreparedStatementInterface {
    public static void main(String args[]) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/java-basic-project-1", "root", "");

            PreparedStatement stmt = con.prepareStatement("insert into emp values(?,?,?)");
            stmt.setInt(1, 101);//1 specifies the first parameter in the query
            stmt.setString(2, "Ratan");
            stmt.setString(3, "2");

            int i = stmt.executeUpdate();
            System.out.println(i + " records inserted");

            con.close();

        } catch (Exception e) {
            System.out.println(e);
        }

    }
}