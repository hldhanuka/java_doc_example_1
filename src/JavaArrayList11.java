import java.util.*;

class JavaArrayList11Book {
    int id;
    String name, author, publisher;
    int quantity;

    public JavaArrayList11Book(int id, String name, String author, String publisher, int quantity) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.quantity = quantity;
    }
}

public class JavaArrayList11 {
    public static void main(String[] args) {
        //Creating list of Books
        List<JavaArrayList11Book> list = new ArrayList<JavaArrayList11Book>();
        //Creating Books
        JavaArrayList11Book b1 = new JavaArrayList11Book(101, "Let us C", "Yashwant Kanetkar", "BPB", 8);
        JavaArrayList11Book b2 = new JavaArrayList11Book(102, "Data Communications and Networking", "Forouzan", "Mc Graw Hill", 4);
        JavaArrayList11Book b3 = new JavaArrayList11Book(103, "Operating System", "Galvin", "Wiley", 6);
        //Adding Books to list
        list.add(b1);
        list.add(b2);
        list.add(b3);
        //Traversing list
        for (JavaArrayList11Book b : list) {
            System.out.println(b.id + " " + b.name + " " + b.author + " " + b.publisher + " " + b.quantity);
        }
    }
}