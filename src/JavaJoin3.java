class JavaJoin3 extends Thread {
    public void run() {
        for (int i = 1; i <= 5; i++) {
            try {
                Thread.sleep(500);
            } catch (Exception e) {
                System.out.println(e);
            }
            System.out.println(i);
        }
    }

    public static void main(String args[]) {
        JavaJoin3 t1 = new JavaJoin3();
        JavaJoin3 t2 = new JavaJoin3();
        JavaJoin3 t3 = new JavaJoin3();
        t1.start();
        try {
            t1.join();
        } catch (Exception e) {
            System.out.println(e);
        }

        t2.start();
        t3.start();
    }
}