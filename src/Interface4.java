interface Interface4Printable {
    void print();
}

interface Interface4Showable extends Interface4Printable {
    void show();
}

class Interface4 implements Interface4Showable {
    public void print() {
        System.out.println("Hello");
    }

    public void show() {
        System.out.println("Welcome");
    }

    public static void main(String args[]) {
        Interface4 obj = new Interface4();
        obj.print();
        obj.show();
    }
}