import java.time.*;

public class JavaMonth2 {
    public static void main(String[] args) {
        Month month = Month.from(LocalDate.now());
        System.out.println(month.getValue());
        System.out.println(month.name());
    }
}