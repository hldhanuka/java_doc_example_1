class SuperKeyword4Animal {
    SuperKeyword4Animal() {
        System.out.println("animal is created");
    }
}

class SuperKeyword4Dog extends SuperKeyword4Animal {
    SuperKeyword4Dog() {
        System.out.println("dog is created");
    }
}

class SuperKeyword4 {
    public static void main(String args[]) {
        SuperKeyword4Dog d = new SuperKeyword4Dog();
    }
}