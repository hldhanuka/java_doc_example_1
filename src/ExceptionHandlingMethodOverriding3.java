import java.io.*;

class ExceptionHandlingMethodOverriding3Parent {
    void msg() throws Exception {
        System.out.println("parent method");
    }
}

class ExceptionHandlingMethodOverriding3 extends ExceptionHandlingMethodOverriding3Parent {
    void msg() throws ArithmeticException {
        System.out.println("child method");
    }

    public static void main(String args[]) {
        ExceptionHandlingMethodOverriding3Parent p = new ExceptionHandlingMethodOverriding3();

        try {
            p.msg();
        } catch (Exception e) {
        }
    }
}