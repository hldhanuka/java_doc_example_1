class SynchronizationWithoutTable {
    void printTable(int n) {//method not synchronized
        for (int i = 1; i <= 5; i++) {
            System.out.println(n * i);
            try {
                Thread.sleep(400);
            } catch (Exception e) {
                System.out.println(e);
            }
        }

    }
}

class SynchronizationWithoutMyThread1 extends Thread {
    SynchronizationWithoutTable t;

    SynchronizationWithoutMyThread1(SynchronizationWithoutTable t) {
        this.t = t;
    }

    public void run() {
        t.printTable(5);
    }

}

class SynchronizationWithoutMyThread2 extends Thread {
    SynchronizationWithoutTable t;

    SynchronizationWithoutMyThread2(SynchronizationWithoutTable t) {
        this.t = t;
    }

    public void run() {
        t.printTable(100);
    }
}

class SynchronizationWithout {
    public static void main(String args[]) {
        SynchronizationWithoutTable obj = new SynchronizationWithoutTable();//only one object
        SynchronizationWithoutMyThread1 t1 = new SynchronizationWithoutMyThread1(obj);
        SynchronizationWithoutMyThread2 t2 = new SynchronizationWithoutMyThread2(obj);
        t1.start();
        t2.start();
    }
}