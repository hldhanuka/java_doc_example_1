import java.time.OffsetDateTime;

public class JavaOffsetDateTime3 {
    public static void main(String[] args) {
        OffsetDateTime offsetDT = OffsetDateTime.now();
        System.out.println(offsetDT.getDayOfWeek());
    }
}