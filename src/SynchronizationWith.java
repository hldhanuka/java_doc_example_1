//example of java synchronized method  
class SynchronizationWithTable {
    synchronized void printTable(int n) {//synchronized method
        for (int i = 1; i <= 5; i++) {
            System.out.println(n * i);
            try {
                Thread.sleep(400);
            } catch (Exception e) {
                System.out.println(e);
            }
        }

    }
}

class SynchronizationWithMyThread1 extends Thread {
    SynchronizationWithTable t;

    SynchronizationWithMyThread1(SynchronizationWithTable t) {
        this.t = t;
    }

    public void run() {
        t.printTable(5);
    }

}

class SynchronizationWithMyThread2 extends Thread {
    SynchronizationWithTable t;

    SynchronizationWithMyThread2(SynchronizationWithTable t) {
        this.t = t;
    }

    public void run() {
        t.printTable(100);
    }
}

public class SynchronizationWith {
    public static void main(String args[]) {
        SynchronizationWithTable obj = new SynchronizationWithTable();//only one object
        SynchronizationWithMyThread1 t1 = new SynchronizationWithMyThread1(obj);
        SynchronizationWithMyThread2 t2 = new SynchronizationWithMyThread2(obj);
        t1.start();
        t2.start();
    }
}