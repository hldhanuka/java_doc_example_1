class RuntimePolymorphism3Bike {
    int speedlimit = 90;
}

class RuntimePolymorphism3 extends RuntimePolymorphism3Bike {
    int speedlimit = 150;

    public static void main(String args[]) {
        RuntimePolymorphism3Bike obj = new RuntimePolymorphism3();
        System.out.println(obj.speedlimit);//90
    }
}