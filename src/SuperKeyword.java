class SuperKeywordAnimal {
    String color = "white";
}

class SuperKeywordDog extends SuperKeywordAnimal {
    String color = "black";

    void printColor() {
        System.out.println(color);//prints color of Dog class  
        System.out.println(super.color);//prints color of Animal class  
    }
}

class SuperKeyword {
    public static void main(String args[]) {
        SuperKeywordDog d = new SuperKeywordDog();
        d.printColor();
    }
}