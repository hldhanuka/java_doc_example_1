interface NestedInterfaceShowable {
    void show();

    interface Message {
        void msg();
    }
}

class NestedInterface implements NestedInterfaceShowable.Message {
    public void msg() {
        System.out.println("Hello nested interface");
    }

    public static void main(String args[]) {
        NestedInterfaceShowable.Message message = new NestedInterface();//upcasting here
        message.msg();
    }
}