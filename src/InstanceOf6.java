interface InstanceOf6Printable {
}

class InstanceOf6A implements InstanceOf6Printable {
    public void a() {
        System.out.println("a method");
    }
}

class InstanceOf6B implements InstanceOf6Printable {
    public void b() {
        System.out.println("b method");
    }
}

class InstanceOf6Call {
    void invoke(InstanceOf6Printable p) {//upcasting
        if (p instanceof InstanceOf6A) {
            InstanceOf6A a = (InstanceOf6A) p;//Downcasting
            a.a();
        }

        if (p instanceof InstanceOf6B) {
            InstanceOf6B b = (InstanceOf6B) p;//Downcasting
            b.b();
        }

    }
}//end of Call class

class InstanceOf6 {
    public static void main(String args[]) {
        InstanceOf6Printable p = new InstanceOf6B();
        InstanceOf6Call c = new InstanceOf6Call();
        c.invoke(p);
    }
}