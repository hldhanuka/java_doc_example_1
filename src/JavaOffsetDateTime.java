import java.time.OffsetDateTime;

public class JavaOffsetDateTime {
    public static void main(String[] args) {
        OffsetDateTime offsetDT = OffsetDateTime.now();
        System.out.println(offsetDT.getDayOfMonth());
    }
}