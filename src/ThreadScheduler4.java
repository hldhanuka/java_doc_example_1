class ThreadScheduler4 extends Thread {
    public void run() {
        for (int i = 1; i < 5; i++) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
            System.out.println(i);
        }
    }

    public static void main(String args[]) {
        ThreadScheduler4 t1 = new ThreadScheduler4();
        ThreadScheduler4 t2 = new ThreadScheduler4();

        t1.run();
        t2.run();
    }
}