import java.util.*;

enum JavaEnumSet2days {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
}

public class JavaEnumSet2 {
    public static void main(String[] args) {
        Set<JavaEnumSet2days> set1 = EnumSet.allOf(JavaEnumSet2days.class);
        System.out.println("Week Days:" + set1);
        Set<JavaEnumSet2days> set2 = EnumSet.noneOf(JavaEnumSet2days.class);
        System.out.println("Week Days:" + set2);
    }
}