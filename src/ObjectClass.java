class ObjectClass implements Cloneable {
    int rollno;
    String name;

    ObjectClass(int rollno, String name) {
        this.rollno = rollno;
        this.name = name;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public static void main(String args[]) {
        try {
            ObjectClass s1 = new ObjectClass(101, "amit");

            ObjectClass s2 = (ObjectClass) s1.clone();

            System.out.println(s1.rollno + " " + s1.name);
            System.out.println(s2.rollno + " " + s2.name);

        } catch (CloneNotSupportedException c) {
        }

    }
}