class SuperKeyword3Animal {
    SuperKeyword3Animal() {
        System.out.println("animal is created");
    }
}

class SuperKeyword3Dog extends SuperKeyword3Animal {
    SuperKeyword3Dog() {
        super();
        System.out.println("dog is created");
    }
}

class SuperKeyword3 {
    public static void main(String args[]) {
        SuperKeyword3Dog d = new SuperKeyword3Dog();
    }
}