//Example of an abstract class that has abstract and non-abstract methods  
abstract class AbstractClass3Bike {
    AbstractClass3Bike() {
        System.out.println("bike is created");
    }

    abstract void run();

    void changeGear() {
        System.out.println("gear changed");
    }
}

//Creating a Child class which inherits Abstract class
class AbstractClass3Honda extends AbstractClass3Bike {
    void run() {
        System.out.println("running safely..");
    }
}

//Creating a Test class which calls abstract and non-abstract methods
class AbstractClass3 {
    public static void main(String args[]) {
        AbstractClass3Bike obj = new AbstractClass3Honda();
        obj.run();
        obj.changeGear();
    }
}