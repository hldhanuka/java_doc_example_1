import java.util.*;

class SortingInCollection4Student implements Comparable<SortingInCollection4Student> {
    public String name;

    public SortingInCollection4Student(String name) {
        this.name = name;
    }

    public int compareTo(SortingInCollection4Student person) {
        return name.compareTo(person.name);

    }
}

public class SortingInCollection4 {
    public static void main(String[] args) {
        ArrayList<SortingInCollection4Student> al = new ArrayList<SortingInCollection4Student>();
        al.add(new SortingInCollection4Student("Viru"));
        al.add(new SortingInCollection4Student("Saurav"));
        al.add(new SortingInCollection4Student("Mukesh"));
        al.add(new SortingInCollection4Student("Tahir"));

        Collections.sort(al);
        for (SortingInCollection4Student s : al) {
            System.out.println(s.name);
        }
    }
}