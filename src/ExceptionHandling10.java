import java.io.*;

class ExceptionHandling10M {
    void method() throws IOException {
        throw new IOException("device error");
    }
}

class ExceptionHandling10 {
    public static void main(String args[]) throws IOException {//declare exception
        ExceptionHandling10M m = new ExceptionHandling10M();
        m.method();

        System.out.println("normal flow...");
    }
}