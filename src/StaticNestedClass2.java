public class StaticNestedClass2 {
    static int data = 30;

    static class StaticNestedClass2Inner {
        static void msg() {
            System.out.println("data is " + data);
        }
    }

    public static void main(String args[]) {
        StaticNestedClass2.StaticNestedClass2Inner.msg();//no need to create the instance of static nested class
    }
}