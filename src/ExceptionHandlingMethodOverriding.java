import java.io.*;

class ExceptionHandlingMethodOverridingParent {
    void msg() {
        System.out.println("parent method");
    }
}

class ExceptionHandlingMethodOverriding extends ExceptionHandlingMethodOverridingParent {
    void msg() throws ArithmeticException {
        System.out.println("child method");
    }

    public static void main(String args[]) {
        ExceptionHandlingMethodOverridingParent p = new ExceptionHandlingMethodOverriding();
        p.msg();
    }
}