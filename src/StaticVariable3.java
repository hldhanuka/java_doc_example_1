//Java  Program  to  illustrate  the  use  of  static  variable  which    
//is  shared  with  all  objects.    
class StaticVariable3 {
    static int count = 0;//will  get  memory  only  once  and  retain  its  value

    StaticVariable3() {
        count++;//incrementing  the  value  of  static  variable    
        System.out.println(count);
    }

    public static void main(String args[]) {
        //creating  objects    
        StaticVariable3 c1 = new StaticVariable3();
        StaticVariable3 c2 = new StaticVariable3();
        StaticVariable3 c3 = new StaticVariable3();
    }
}