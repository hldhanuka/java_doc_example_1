//Java Program to illustrate the use of Java Method Overriding  
//Creating a parent class.  
class Vehicle2 {
    //defining a method
    void run() {
        System.out.println("Vehicle is running");
    }
}

//Creating a child class  
class MethodOverriding2 extends Vehicle2 {
    //defining the same method as in the parent class
    void run() {
        System.out.println("Bike is running safely");
    }

    public static void main(String args[]) {
        MethodOverriding2 obj = new MethodOverriding2();//creating object
        obj.run();//calling method
    }
}