import java.time.Clock;

public class JavaClock {
    public static void main(String[] args) {
        Clock c = Clock.systemDefaultZone();
        System.out.println(c.getZone());
    }
}