class AAA1 {
    AAA1 foo() {
        return this;
    }

    void print() {
        System.out.println("Inside the class A1");
    }
}


// A2 is the child class of A1  
class AAA2 extends AAA1 {
    @Override
    AAA2 foo() {
        return this;
    }

    void print() {
        System.out.println("Inside the class A2");
    }
}

// A3 is the child class of A2  
class AAA3 extends AAA2 {
    @Override
    AAA3 foo() {
        return this;
    }

    @Override
    void print() {
        System.out.println("Inside the class A3");
    }
}

public class CovariantReturnType3 {
    // main method
    public static void main(String argvs[]) {
        AAA1 a1 = new AAA1();

        a1.foo().print();

        AAA2 a2 = new AAA2();

        a2.foo().print();

        AAA3 a3 = new AAA3();

        a3.foo().print();

    }
}