import java.time.*;
import java.time.temporal.Temporal;

public class JavaZoneOffset {
    public static void main(String[] args) {
        ZoneOffset zone = ZoneOffset.UTC;
        Temporal temp = zone.adjustInto(ZonedDateTime.now());
        System.out.println(temp);
    }
}