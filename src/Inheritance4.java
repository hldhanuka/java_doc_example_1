class Animal4 {
    void eat() {
        System.out.println("eating...");
    }
}

class Dog4 extends Animal4 {
    void bark() {
        System.out.println("barking...");
    }
}

class Cat4 extends Animal4 {
    void meow() {
        System.out.println("meowing...");
    }
}

class Inheritance4 {
    public static void main(String args[]) {
        Cat4 c = new Cat4();
        c.meow();
        c.eat();
        //c.bark();//C.T.Error    
    }
}