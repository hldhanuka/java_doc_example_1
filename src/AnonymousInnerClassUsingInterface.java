interface AnonymousInnerClassUsingInterfaceEatable {
    void eat();
}

class AnonymousInnerClassUsingInterface {
    public static void main(String args[]) {
        AnonymousInnerClassUsingInterfaceEatable e = new AnonymousInnerClassUsingInterfaceEatable() {
            public void eat() {
                System.out.println("nice fruits");
            }
        };
        e.eat();
    }
}