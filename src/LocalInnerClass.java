public class LocalInnerClass {
    private int data = 30;//instance variable

    void display() {
        class LocalInnerClassLocal {
            void msg() {
                System.out.println(data);
            }
        }
        LocalInnerClassLocal l = new LocalInnerClassLocal();
        l.msg();
    }

    public static void main(String args[]) {
        LocalInnerClass obj = new LocalInnerClass();
        obj.display();
    }
}