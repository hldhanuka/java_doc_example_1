class RuntimePolymorphism5Animal {
    void eat() {
        System.out.println("animal is eating...");
    }
}

class RuntimePolymorphism5Dog extends RuntimePolymorphism5Animal {
    void eat() {
        System.out.println("dog is eating...");
    }
}

class RuntimePolymorphism5 extends RuntimePolymorphism5Dog {
    public static void main(String args[]) {
        RuntimePolymorphism5Animal a = new RuntimePolymorphism5();
        a.eat();
    }
}