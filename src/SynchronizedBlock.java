class SynchronizedBlockTable {
    void printTable(int n) {
        synchronized (this) {//synchronized block
            for (int i = 1; i <= 5; i++) {
                System.out.println(n * i);
                try {
                    Thread.sleep(400);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
    }//end of the method
}

class SynchronizedBlockMyThread1 extends Thread {
    SynchronizedBlockTable t;

    SynchronizedBlockMyThread1(SynchronizedBlockTable t) {
        this.t = t;
    }

    public void run() {
        t.printTable(5);
    }

}

class SynchronizedBlockMyThread2 extends Thread {
    SynchronizedBlockTable t;

    SynchronizedBlockMyThread2(SynchronizedBlockTable t) {
        this.t = t;
    }

    public void run() {
        t.printTable(100);
    }
}

public class SynchronizedBlock {
    public static void main(String args[]) {
        SynchronizedBlockTable obj = new SynchronizedBlockTable();//only one object
        SynchronizedBlockMyThread1 t1 = new SynchronizedBlockMyThread1(obj);
        SynchronizedBlockMyThread2 t2 = new SynchronizedBlockMyThread2(obj);
        t1.start();
        t2.start();
    }
}