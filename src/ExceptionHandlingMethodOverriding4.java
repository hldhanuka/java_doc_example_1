import java.io.*;

class ExceptionHandlingMethodOverriding4Parent {
    void msg() throws Exception {
        System.out.println("parent method");
    }
}

class ExceptionHandlingMethodOverriding4 extends ExceptionHandlingMethodOverriding4Parent {
    void msg() {
        System.out.println("child method");
    }

    public static void main(String args[]) {
        ExceptionHandlingMethodOverriding4Parent p = new ExceptionHandlingMethodOverriding4();

        try {
            p.msg();
        } catch (Exception e) {
        }

    }
}