//Program of synchronized method by using annonymous class  
class SynchronizationWith2Table {
    synchronized void printTable(int n) {//synchronized method
        for (int i = 1; i <= 5; i++) {
            System.out.println(n * i);
            try {
                Thread.sleep(400);
            } catch (Exception e) {
                System.out.println(e);
            }
        }

    }
}

public class SynchronizationWith2 {
    public static void main(String args[]) {
        final SynchronizationWith2Table obj = new SynchronizationWith2Table();//only one object

        Thread t1 = new Thread() {
            public void run() {
                obj.printTable(5);
            }
        };
        Thread t2 = new Thread() {
            public void run() {
                obj.printTable(100);
            }
        };

        t1.start();
        t2.start();
    }
}