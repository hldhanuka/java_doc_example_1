class RuntimePolymorphismBike {
    void run() {
        System.out.println("running");
    }
}

class RuntimePolymorphism extends RuntimePolymorphismBike {
    void run() {
        System.out.println("running safely with 60km");
    }

    public static void main(String args[]) {
        RuntimePolymorphismBike b = new RuntimePolymorphism();//upcasting
        b.run();
    }
}