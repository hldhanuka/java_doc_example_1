class SuperKeyword2Animal {
    void eat() {
        System.out.println("eating...");
    }
}

class SuperKeyword2Dog extends SuperKeyword2Animal {
    void eat() {
        System.out.println("eating bread...");
    }

    void bark() {
        System.out.println("barking...");
    }

    void work() {
        super.eat();
        bark();
    }
}

class SuperKeyword2 {
    public static void main(String args[]) {
        SuperKeyword2Dog d = new SuperKeyword2Dog();
        d.work();
    }
}