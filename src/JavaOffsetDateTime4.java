import java.time.OffsetDateTime;

public class JavaOffsetDateTime4 {
    public static void main(String[] args) {
        OffsetDateTime offsetDT = OffsetDateTime.now();
        System.out.println(offsetDT.toLocalDate());
    }
}