import java.io.*;

public class JavaReader {
    public static void main(String[] args) {
        try {
            Reader reader = new FileReader("JavaReaderfile.txt");
            int data = reader.read();
            while (data != -1) {
                System.out.print((char) data);
                data = reader.read();
            }
            reader.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}