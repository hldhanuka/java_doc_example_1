import java.util.*;

class JavaLinkedList5Book {
    int id;
    String name, author, publisher;
    int quantity;

    public JavaLinkedList5Book(int id, String name, String author, String publisher, int quantity) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.quantity = quantity;
    }
}

public class JavaLinkedList5 {
    public static void main(String[] args) {
        //Creating list of Books
        List<JavaLinkedList5Book> list = new LinkedList<JavaLinkedList5Book>();
        //Creating Books
        JavaLinkedList5Book b1 = new JavaLinkedList5Book(101, "Let us C", "Yashwant Kanetkar", "BPB", 8);
        JavaLinkedList5Book b2 = new JavaLinkedList5Book(102, "Data Communications & Networking", "Forouzan", "Mc Graw Hill", 4);
        JavaLinkedList5Book b3 = new JavaLinkedList5Book(103, "Operating System", "Galvin", "Wiley", 6);
        //Adding Books to list
        list.add(b1);
        list.add(b2);
        list.add(b3);
        //Traversing list
        for (JavaLinkedList5Book b : list) {
            System.out.println(b.id + " " + b.name + " " + b.author + " " + b.publisher + " " + b.quantity);
        }
    }
}