import java.io.*;

public class JavaTransientKeywordStudent implements Serializable {
    int id;
    String name;
    transient int age;//Now it will not be serialized

    public JavaTransientKeywordStudent(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
}

class JavaTransientKeyword {
    public static void main(String args[]) throws Exception {
        JavaTransientKeywordStudent s1 = new JavaTransientKeywordStudent(211, "ravi", 22);//creating object
        //writing object into file
        FileOutputStream f = new FileOutputStream("JavaTransientKeywordf.txt");
        ObjectOutputStream out = new ObjectOutputStream(f);
        out.writeObject(s1);
        out.flush();
        out.close();
        f.close();
        System.out.println("success");
    }
}