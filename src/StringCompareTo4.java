// import statement  

import java.util.*;

class StringCompareTo4Players {

    private String name;

    // constructor of the class
    public StringCompareTo4Players(String str) {
        name = str;
    }

}

public class StringCompareTo4 {

    // main method
    public static void main(String[] args) {

        StringCompareTo4Players ronaldo = new StringCompareTo4Players("Ronaldo");
        StringCompareTo4Players sachin = new StringCompareTo4Players("Sachin");
        StringCompareTo4Players messi = new StringCompareTo4Players("Messi");
        ArrayList<StringCompareTo4Players> al = new ArrayList<>();

        al.add(ronaldo);
        al.add(sachin);
        al.add(messi);

        // performing binary search on the list al  
        Collections.binarySearch(al, "Sehwag", null);
    }

}