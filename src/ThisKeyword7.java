class B {
    ThisKeyword7 obj;

    B(ThisKeyword7 obj) {
        this.obj = obj;
    }

    void display() {
        System.out.println(obj.data);//using  data  member  of  A4  class
    }
}

class ThisKeyword7 {
    int data = 10;

    ThisKeyword7() {
        B b = new B(this);
        b.display();
    }

    public static void main(String args[]) {
        ThisKeyword7 a = new ThisKeyword7();
    }
}