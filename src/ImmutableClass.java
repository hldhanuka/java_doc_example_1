final class ImmutableClassEmployee {
    final String pancardNumber;

    public ImmutableClassEmployee(String pancardNumber) {
        this.pancardNumber = pancardNumber;
    }

    public String getPancardNumber() {
        return pancardNumber;
    }
}

public class ImmutableClass {
    public static void main(String ar[]) {
        ImmutableClassEmployee e = new ImmutableClassEmployee("ABC123");
        String s1 = e.getPancardNumber();
        System.out.println("Pancard Number: " + s1);
    }
}