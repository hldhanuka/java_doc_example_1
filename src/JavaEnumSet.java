import java.util.*;

enum JavaEnumSetdays {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
}

public class JavaEnumSet {
    public static void main(String[] args) {
        Set<JavaEnumSetdays> set = EnumSet.of(JavaEnumSetdays.TUESDAY, JavaEnumSetdays.WEDNESDAY);
        // Traversing elements
        Iterator<JavaEnumSetdays> iter = set.iterator();
        while (iter.hasNext())
            System.out.println(iter.next());
    }
}