abstract class AnonymousInnerClassPerson {
    abstract void eat();
}

class AnonymousInnerClass {
    public static void main(String args[]) {
        AnonymousInnerClassPerson p = new AnonymousInnerClassPerson() {
            void eat() {
                System.out.println("nice fruits");
            }
        };
        p.eat();
    }
}