class LocalInnerClass2 {
    private int data = 30;//instance variable

    void display() {
        int value = 50;//local variable must be final till jdk 1.7 only
        class LocalInnerClass2Local {
            void msg() {
                System.out.println(value);
            }
        }
        LocalInnerClass2Local l = new LocalInnerClass2Local();
        l.msg();
    }

    public static void main(String args[]) {
        LocalInnerClass2 obj = new LocalInnerClass2();
        obj.display();
    }
}