import java.sql.*;

class JavaDatabaseBatchProcessing {
    public static void main(String args[]) throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/java-basic-project-1", "root", "");
        con.setAutoCommit(false);

        Statement stmt = con.createStatement();
        stmt.addBatch("insert into emp values(190,'abhi',40000)");
        stmt.addBatch("insert into emp values(191,'umesh',50000)");

        stmt.executeBatch();//executing the batch  

        con.commit();
        con.close();
    }
}