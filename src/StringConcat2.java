// A Java program that shows how to add   
// a string at the beginning of another string  
public class StringConcat2 {
    // main method
    public static void main(String argvs[]) {
        String str = "Country";

        // we have added the string "India is my" before the String str;  
// Also, observe that a string literal can also invoke the concat() method  
        String s = "India is my ".concat(str);

        // displaying the string  
        System.out.println(s);

    }
}